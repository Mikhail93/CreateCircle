var size = '';
var colorRed = '';
var colorGreen = '';
var colorBlue = '';
/*var color = '';*/

function setParam() {
    var modalWindow = document.createElement('div');
    modalWindow.className = 'modalWindow';

    var inputDiameter = document.createElement('input');
    inputDiameter.type = 'text';
    inputDiameter.className = 'setSize d-block';
    inputDiameter.placeholder = 'Input diameter';
    modalWindow.appendChild(inputDiameter);

    /*var inputColor = document.createElement('input');
    inputColor.type = 'text';
    inputColor.className = 'setColor d-block';
    inputColor.placeholder = 'Input color (RGB)';
    modalWindow.appendChild(inputColor);*/

    var inputColorRed = document.createElement('input');
    inputColorRed.type = 'text';
    inputColorRed.className = 'setColor';
    inputColorRed.placeholder = 'Red';
    modalWindow.appendChild(inputColorRed);

    var inputColorGreen = document.createElement('input');
    inputColorGreen.type = 'text';
    inputColorGreen.className = 'setColor';
    inputColorGreen.placeholder = 'Green';
    modalWindow.appendChild(inputColorGreen);

    var inputColorBlue = document.createElement('input');
    inputColorBlue.type = 'text';
    inputColorBlue.className = 'setColor';
    inputColorBlue.placeholder = 'Blue';
    modalWindow.appendChild(inputColorBlue);


    var btnSubmit = document.createElement('button');
    btnSubmit.type = 'submit';
    btnSubmit.className = 'btn btn-success btnSubmit d-block';
    btnSubmit.innerHTML = 'Create';
    btnSubmit.onclick = function () {
        size = inputDiameter.value;
        colorRed = inputColorRed.value;
        colorGreen = inputColorGreen.value;
        colorBlue = inputColorBlue.value;
        createCircle(size, colorRed, colorGreen, colorBlue, modalWindow);
    };
    modalWindow.appendChild(btnSubmit);

    var param = document.getElementsByClassName('btn')[0];
    document.getElementById('container').replaceChild(modalWindow, param);
}

function createCircle(size, colorRed, colorGreen, colorBlue, replace) {
    var containerCircle = document.createElement('div');
    containerCircle.className = 'container-circle';

    var btnDelete = document.createElement('button');
    btnDelete.className = 'btn btn-danger';
    btnDelete.innerHTML = 'Delete';
    btnDelete.onclick = function () {
        reboot(containerCircle);
    };
    containerCircle.appendChild(btnDelete);

    var circle = document.createElement('div');
    circle.style.backgroundColor = 'rgb(' + colorRed + ', ' + colorGreen + ', ' + colorBlue + ')';
    circle.style.width = size + 'px';
    circle.style.height = size + 'px';
    circle.style.borderRadius = '50%';
    containerCircle.appendChild(circle);

    document.getElementById('container').replaceChild(containerCircle, replace);
}

function reboot(containerCircle) {
    var btnStart = document.createElement('button');
    btnStart.type = 'button';
    btnStart.className = 'btn btn-warning';
    btnStart.onclick = function () {
        setParam();
    };
    btnStart.innerHTML = 'Create circle';

    document.getElementById('container').replaceChild(btnStart, containerCircle);
}